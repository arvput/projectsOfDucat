package com.example.vikashrajput.startanotheractivity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText e1,e2,e3;
    Button b1;
    public static final String EXTRA_NMAE="";
    public static final String EXTRA_PHONE="";
    public static final String EXTRA_ADDRESS="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        e1= (EditText) findViewById(R.id.editTextName);
        b1= (Button) findViewById(R.id.button);
        e2= (EditText) findViewById(R.id.editTextPhone);
        e3= (EditText) findViewById(R.id.editTextAddress);

    }

    public void sendMessage(View view)
    {
        //
        // Intent intent=new Intent(this, DisplayMessageActivity.class);
        Intent intent=new Intent(Intent.ACTION_WEB_SEARCH, Uri.parse("www.google.com"));
        String name=e1.getText().toString();
        String phone=e2.getText().toString();
        String address=e3.getText().toString();

        intent.putExtra(EXTRA_NMAE, name);
        intent.putExtra(EXTRA_PHONE,phone);
        intent.putExtra(EXTRA_ADDRESS,address);
        startActivity(intent);
    }

}
