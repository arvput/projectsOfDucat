package com.example.vikashrajput.smsservice;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText e1,e2;
    Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        e1= (EditText) findViewById(R.id.editText);
        e2= (EditText) findViewById(R.id.editText2);
        btn= (Button) findViewById(R.id.button);
        if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this,new String[] {Manifest.permission.SEND_SMS},0);
            Toast.makeText(this, "if statment execute", Toast.LENGTH_SHORT).show();
            return;
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "SMS SEnd Sussfully", Toast.LENGTH_SHORT).show();

                String phone=e1.getText().toString();
                String msg=e2.getText().toString();
                SmsManager sm=SmsManager.getDefault();
                sm.sendTextMessage(phone,null,msg,null,null);
                Toast.makeText(MainActivity.this, "SMS SEnd Sussfully", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
