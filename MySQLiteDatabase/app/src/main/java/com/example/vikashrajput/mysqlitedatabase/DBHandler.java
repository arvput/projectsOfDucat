package com.example.vikashrajput.mysqlitedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikashrajput on 7/27/17.
 */

public class DBHandler extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;
    private static final String DATAB   ASE_NAME = "mydb";
    private static final String TABLE_EMPLOYEE = "emp_record";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMP_ADDRESS = "address";
    private static final String TAG = "ss";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       // String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_EMPLOYEE + " (" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_EMP_ADDRESS + " TEXT " + ")";
        //sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_EMPLOYEE + " ("+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_EMP_ADDRESS + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_LOGIN_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEE);
        onCreate(sqLiteDatabase);
    }

    public void addEmployee(Employee employee) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, employee.getId());
        values.put(KEY_NAME, employee.getName());
        values.put(KEY_EMP_ADDRESS, employee.getAddress());
        long insert = db.insert(TABLE_EMPLOYEE, null, values);

        db.close();
    }

    public Employee getEmployee(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
       // Cursor cursor = db.query(TABLE_EMPLOYEE, new String[] {KEY_ID, KEY_NAME, KEY_EMP_ADDRESS}, KEY_ID+"=?", new String[]{String.valueOf(id)}, null, null, null, null);
        Cursor cursor=db.query(TABLE_EMPLOYEE, new String[] {KEY_ID, KEY_NAME, KEY_EMP_ADDRESS}, KEY_ID+ "=?", new String[]{String.valueOf(id)}, null,null,null,null);
        if (cursor!=null) {
            cursor.moveToFirst();
        }
        Employee contact = new Employee(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
        cursor.close();
        return contact;
    }

    public List<Employee> getAllEmployee() {
        List<Employee> employeeList = new ArrayList<Employee>();
        String selectQuery = "SELECT * FROM " + TABLE_EMPLOYEE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Employee employee = new Employee();
                employee.setId(Integer.parseInt(cursor.getString(0)));
                employee.setName(cursor.getString(1));
                employee.setAddress(cursor.getString(2));
                employeeList.add(employee);
            }
            while (cursor.moveToNext());
        }
        return employeeList;
    }

    public int updateEmployee(Employee employee)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME, employee.getName());
        values.put(KEY_EMP_ADDRESS, employee.getAddress());

        return db.update(TABLE_EMPLOYEE, values, KEY_ID+ "=?", new String[]{String.valueOf(employee.getId())});
    }

    public void deleteEmployeeRec(int id)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_EMPLOYEE, KEY_ID+ "=?", new String[]{String.valueOf(id)});
        db.close();
    }
}
