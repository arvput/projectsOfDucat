package com.example.vikashrajput.mysqlitedatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText e1,e2,e3;
    Button b1,b2,b3,b4,b5;
    DBHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        e1= (EditText) findViewById(R.id.editText);
        e2= (EditText) findViewById(R.id.editText2);
        e3= (EditText) findViewById(R.id.editText3);
        b1= (Button) findViewById(R.id.button);
        b2= (Button) findViewById(R.id.button2);
        b3= (Button) findViewById(R.id.button3);
        b4= (Button) findViewById(R.id.button4);
        b5= (Button) findViewById(R.id.button5);
        db=new DBHandler(this);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id=Integer.parseInt(e1.getText().toString());
                String name=e2.getText().toString();
                String address=e3.getText().toString();

                db.addEmployee(new Employee(id, name, address));
                Toast.makeText(MainActivity.this, "DATA SAVEd", Toast.LENGTH_SHORT).show();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id=Integer.parseInt(e1.getText().toString());
                Employee employee=db.getEmployee(id);
                Toast.makeText(MainActivity.this, "hello", Toast.LENGTH_SHORT).show();
                //Toast.makeText(MainActivity.this, "Id:-"+employee.getId()+ "\nName :-"+employee.getName()+ "\nAddress:- "+employee.getAddress(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
