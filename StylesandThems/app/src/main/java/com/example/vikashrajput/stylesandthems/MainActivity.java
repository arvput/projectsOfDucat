package com.example.vikashrajput.stylesandthems;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    Button btn;
    CharSequence[] items={"google", "Apple", "Microsoft"};
    boolean[] itemsChecked=new boolean[items.length];

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
        btn = (Button) findViewById(R.id.button);
        showDialog(0);


    }
    public void onClick(View view)
    {
        showDialog(1);
    }
    protected Dialog onCreateDialog(int id) {
        switch (id)
        {
            case 0:
                return new AlertDialog.Builder(this) .setIcon(R.drawable.abc).setTitle("This is a dialog with some simple text...").setPositiveButton("OK",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            Toast.makeText(getBaseContext(),
                                    "OK clicked!", Toast.LENGTH_SHORT).show();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            Toast.makeText(MainActivity.this, "Cancel mm clicked!", Toast.LENGTH_SHORT).show();
                        }
                }).setMultiChoiceItems(items, itemsChecked, new DialogInterface.OnMultiChoiceClickListener()
                        {
                          public void onClick(DialogInterface dialog, int which, boolean isChecked)
                          {
                            Toast.makeText(getBaseContext(),
                                    items[which] + (isChecked ? " checked !":"unchecked !"),
                                Toast.LENGTH_SHORT).show();
                          }
                        }).create();
            case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon(R.drawable.abc);
                builder.setTitle("This is my Dialog ");
                builder.setPositiveButton("OKKKKKK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Okkk clicked", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("Cancleeee", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Canlceeeeee clicked", Toast.LENGTH_SHORT).show();
                    }
                });
                return builder.create();
        }
        return null;
    }
  /*      AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.abc);
        builder.setTitle("This is my Dialog ");
        builder.setPositiveButton("OKKKKKK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Okkk clicked", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancleeee", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Canlceeeeee clicked", Toast.LENGTH_SHORT).show();
            }
        });
        return builder.create();
    }*/

}


