package com.example.vikashrajput.studentmanagmentsystem;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeleteStudentData extends AppCompatActivity {
    EditText editId;
    Button btnDelete;
    DBHandler myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_student_data);
        editId= (EditText) findViewById(R.id.editId);
        btnDelete= (Button) findViewById(R.id.btnDelete);
        myDb=new DBHandler(this);
        deleteData1();
    }
    public void deleteData1()
    {
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer deleteRows= myDb.deleteData(editId.getText().toString());
                if(deleteRows > 0)
                {
                    showDialog("Warning !","Data Of student Id "+editId.getText().toString()+" Sussfully Deleted");
                }
                else
                    showDialog("Warning !","Data Of student Id "+editId.getText().toString()+" Not Found");

            }
        });
    }
    public void showDialog(String title,String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton("Close",null);
        builder.show();
    }
}
