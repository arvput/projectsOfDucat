package com.example.vikashrajput.studentmanagmentsystem;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    ImageButton addbtn,showbtn,deletebtn,updatebtn;
    Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addbtn= (ImageButton) findViewById(R.id.addbtn);
        showbtn= (ImageButton) findViewById(R.id.showbtn);
        deletebtn= (ImageButton) findViewById(R.id.deletebtn);
        updatebtn= (ImageButton) findViewById(R.id.updatebtn);
        // Drawable d=getResources().getDrawable(R.drawable.actionbar_image);
        //  getActionBar().setBackgroundDrawable(d);

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i=new Intent(MainActivity.this,AddStudentsData.class);
                startActivity(i);
            }
        });
        showbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i=new Intent(MainActivity.this,StudentData.class);
                startActivity(i);
            }
        });
        updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i=new Intent(MainActivity.this,UpdateStudentDetails.class);
                startActivity(i);
            }
        });
        deletebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i=new Intent(MainActivity.this,DeleteStudentData.class);
                startActivity(i);
            }
        });

    }
}
