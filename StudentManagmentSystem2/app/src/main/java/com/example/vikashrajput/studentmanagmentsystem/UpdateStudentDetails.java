package com.example.vikashrajput.studentmanagmentsystem;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateStudentDetails extends AppCompatActivity {
    Button btnUpdate,btnShowRecord;
    EditText editId,editName,editAddress,editPhone;
    DBHandler myDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_student_details);
        btnUpdate= (Button) findViewById(R.id.btnUpdate);
        btnShowRecord= (Button) findViewById(R.id.btnShowRecord);
        editId= (EditText) findViewById(R.id.editId);
        editName= (EditText) findViewById(R.id.editName);
        editAddress= (EditText) findViewById(R.id.editAddress);
        editPhone= (EditText) findViewById(R.id.editPhone);
        editPhone.setVisibility(View.INVISIBLE);
        editAddress.setVisibility(View.INVISIBLE);
        editName.setVisibility(View.INVISIBLE);

        myDb=new DBHandler(this);
        updateData();
        showRecord();
    }
    public void updateData()
    {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isUpdated= myDb.updateData(editId.getText().toString(),editName.getText().toString(),editAddress.getText().toString(),editPhone.getText().toString());
                editName.setText("");
                editAddress.setText("");
                editPhone.setText("");
                editPhone= (EditText) findViewById(R.id.editPhone);
                editPhone.setVisibility(View.INVISIBLE);
                editAddress.setVisibility(View.INVISIBLE);
                editName.setVisibility(View.INVISIBLE);

            }
        });
     /*   btnShowRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPhone.setVisibility(View.VISIBLE);
                editAddress.setVisibility(View.VISIBLE);
                editName.setVisibility(View.VISIBLE);

            }
        });*/
    }
    public void showRecord()
    {
        {
            btnShowRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editPhone.setVisibility(View.VISIBLE);
                    editAddress.setVisibility(View.VISIBLE);
                    editName.setVisibility(View.VISIBLE);
                    String id=editId.getText().toString();
                    Cursor res= myDb.getAllData();
                    if(res.getCount()==0)
                    {
                        showMessage("Error ","nothing found");
                        return;
                    }
                    StringBuffer buffer=new StringBuffer();
                    if(id==null)
                    {
                        Toast.makeText(UpdateStudentDetails.this, "Erooooooooo", Toast.LENGTH_SHORT).show();
                    }
                    while (res.moveToNext())
                    {

                        editName.setText("");
                        editAddress.setText("");
                        editPhone.setText("");
                        // TO GET ALL THE DATA FROM THE DATABASE
                /*    buffer.append("Id :"+res.getString(0)+"\n");
                    buffer.append("name :"+res.getString(1)+"\n");
                    buffer.append("address :"+res.getString(2)+"\n");
                    buffer.append("phone :"+res.getString(3)+"\n\n");*/
                        String dbid=res.getString(0);
                        String name=res.getString(1);
                        String address= res.getString(2);
                        String phone=res.getString(3);
                        editName.setText("");
                        editAddress.setText(address);
                        editPhone.setText(phone);
                        if(id.equals(dbid))
                        {
                            editName.setText(name);
                            editAddress.setText(address);
                            editPhone.setText(phone);
                            break;
                        }
                    }
                }
            });
        }

    }
    public void showMessage(String title,String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

}
