package com.example.vikashrajput.studentmanagmentsystem;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//import static com.example.vikashrajput.studentmanagmentsystem.R.id.editMarks;
//import static com.example.vikashrajput.studentmanagmentsystem.R.id.editSurname;

public class AddStudentsData extends AppCompatActivity {
    EditText editName,editAddress,editPhone,editId;
    Button btnAdd;
    DBHandler myDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_students_data);
        editId= (EditText) findViewById(R.id.editId);
        editName= (EditText) findViewById(R.id.editName);
        editAddress= (EditText) findViewById(R.id.editAddress);
        editPhone= (EditText) findViewById(R.id.editPhone);

        btnAdd= (Button) findViewById(R.id.btnAdd);
        myDb=new DBHandler(this);
        addData();
    }
    public void addData()
    {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isInserted  = myDb.insertData(editId.getText().toString(),editName.getText().toString(),editAddress.getText().toString(),editPhone.getText().toString());
                if(isInserted==true)
                {
                    Toast.makeText(AddStudentsData.this, "Data Inserted ", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(AddStudentsData.this, "Data not inserted ", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
