package com.example.vikashrajput.notificationtest;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMassengingService extends FirebaseMessagingService {

    private static final String TAG= "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG,"From: "+remoteMessage.getFrom());
        Log.d(TAG,"Notification MEssage body : "+remoteMessage.getNotification().getBody());

    }
}
