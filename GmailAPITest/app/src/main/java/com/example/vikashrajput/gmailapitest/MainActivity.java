package com.example.vikashrajput.gmailapitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MainActivity extends AppCompatActivity {
    EditText editTo,editSubject,editMessage;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editMessage= (EditText) findViewById(R.id.editMessage);
        editSubject= (EditText) findViewById(R.id.editSubject);
        editTo= (EditText) findViewById(R.id.EditTo);
        btn= (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread t=new Thread(){
                    public void run(){
                        String to=editTo.getText().toString();
                        String sub=editSubject.getText().toString();
                        String msg=editMessage.getText().toString();
                        String from="arvput44@gmail.com";
                        String password="9536856891ARV";
                        String host="smtp.gmail.com";
                        try {
                            Properties p=new Properties();
                            Session sess=Session.getInstance(p);
                            MimeMessage m=new MimeMessage(sess);
                            InternetAddress toId=new InternetAddress(to);
                            InternetAddress fromId=new InternetAddress(from);
                            m.setFrom(fromId);
                            m.setRecipient(Message.RecipientType.TO,toId);
                            m.setSubject(sub);
                            m.setText(msg);
                            Transport tpt = sess.getTransport("smtp");
                            tpt.connect(host,from,password);
                            tpt.sendMessage(m,m.getAllRecipients());
                            tpt.close();

                        } catch (AddressException e) {
                            e.printStackTrace();
                        } catch (MessagingException e) {
                            e.printStackTrace();
                        }
                    }
                };
                t.start();;
            }
        });
    }
}
