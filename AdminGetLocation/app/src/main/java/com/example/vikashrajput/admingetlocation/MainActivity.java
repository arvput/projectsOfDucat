package com.example.vikashrajput.admingetlocation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    Button button;
    FirebaseDatabase database;
    DatabaseReference reference;
    Double lat,longi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView= (TextView) findViewById(R.id.textView);
        button= (Button) findViewById(R.id.button);
        database=FirebaseDatabase.getInstance();
        reference=database.getReference("location");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LocationFnder lf= (LocationFnder) dataSnapshot.getValue();
                lat=lf.getLatitude();
                longi=lf.getLongitude();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MapsActivity().getLocation(lat,longi);
                startActivity(new Intent(MainActivity.this,MapsActivity.class));
            }
        });

    }
}
