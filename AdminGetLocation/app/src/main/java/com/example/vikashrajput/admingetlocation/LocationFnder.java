package com.example.vikashrajput.admingetlocation;

/**
 * Created by vikashrajput on 9/11/17.
 */

public class LocationFnder {
    Double latitude,longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
