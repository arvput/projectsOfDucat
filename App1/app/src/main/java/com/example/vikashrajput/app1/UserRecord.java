package com.example.vikashrajput.app1;

/**
 * Created by vikashrajput on 9/11/17.
 */

public class UserRecord {
    String uName,uPass;
    public  UserRecord()
    {

    }
    public UserRecord(String uName,String uPass)
    {
        this.uName=uName;
        this.uPass=uPass;

    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPass() {
        return uPass;
    }

    public void setuPass(String uPass) {
        this.uPass = uPass;
    }

}
