package com.example.vikashrajput.datetimeassigment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView tvName,tvAddress,tvPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        tvAddress= (TextView) findViewById(R.id.textView2);
        tvName= (TextView) findViewById(R.id.textView);
        tvPhone= (TextView) findViewById(R.id.textView3);
        Intent intent=getIntent();
        String nm=intent.getStringExtra(MainActivity.NAME);
        String add=intent.getStringExtra(MainActivity.ADDRESS);
        String ph=intent.getStringExtra(MainActivity.PHONE);

        tvName.setText(nm);
        tvPhone.setText(ph);
        tvAddress.setText(add);
    }
}
