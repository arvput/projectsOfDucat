package com.example.vikashrajput.datetimeassigment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.R.id.message;

public class MainActivity extends AppCompatActivity {
    EditText name,ad,phone;
    Button btn;
    public static final String NAME ="";
    public static final String ADDRESS ="";
    public static final String PHONE="";
    String str[]=new String[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = (EditText) findViewById(R.id.edName);
        ad = (EditText) findViewById(R.id.edAddress);
        phone = (EditText) findViewById(R.id.edPhone);
        btn = (Button) findViewById(R.id.button);
     /*   btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SecondActivity sa=new SecondActivity();
                Intent intent;
                intent = new Intent();
                String nm=name.getText().toString();
                String add=ad.getText().toString();
                String ph=phone.getText().toString();
                intent.putExtra(NAME, nm);
                intent.putExtra(NAME, add);
                intent.putExtra(NAME, ph);
                startActivity(intent);
            }
        });*/
    }
    public void sendMessage(View view)
    {
        Intent intent;
        intent = new Intent(this, SecondActivity.class);
        String nm=name.getText().toString();
        String add=ad.getText().toString();
        String ph=phone.getText().toString();
        intent.putExtra(NAME, nm);
        intent.putExtra(PHONE, ph);
        intent.putExtra(ADDRESS, add);
        startActivity(intent);
    }
}
