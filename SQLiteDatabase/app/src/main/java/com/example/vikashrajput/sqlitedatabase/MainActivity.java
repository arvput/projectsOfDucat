package com.example.vikashrajput.sqlitedatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity
{
    EditText e1,e2,e3;
    Button btn1,btn2,btn3;
    DBHalper db;
    Employee employee;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        e1= (EditText) findViewById(R.id.editText);
        e2= (EditText) findViewById(R.id.editText3);
        e3= (EditText) findViewById(R.id.editText4);
        btn1= (Button) findViewById(R.id.button);
        btn2= (Button) findViewById(R.id.button2);
        btn3= (Button) findViewById(R.id.button3);
        db=new DBHalper(this);

        btn1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int id=Integer.parseInt(e1.getText().toString());
                String name=e2.getText().toString();
                String address=e3.getText().toString();
                db.addEmployee(new Employee(id, name, address));
                Toast.makeText(MainActivity.this, "Data Saved ", Toast.LENGTH_SHORT).show();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int id=Integer.parseInt(e1.getText().toString());
                 employee = db.getEmployee(id);

                //Toast.makeText(MainActivity.this, "ID:-"+employee.getId()+"\nName:-"+employee.getName()+"\nAddress:- "+employee.getAddress(), Toast.LENGTH_SHORT).show();
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Employee>e=db.getAllEmployee();
                for(Employee emp:e)
                {
                    Toast.makeText(MainActivity.this, "ID:-"+employee.getId() +"\nName:-"+employee.getName() +"\nAddress:-"+employee.getAddress(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
