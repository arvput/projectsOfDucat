package com.example.vikashrajput.sqlitedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikashrajput on 7/26/17.
 */

public class DBHalper extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="mydb";
    private static final String TABLE_EMPLOYEE="emp_record";
    private static final String KEY_ID="id";
    private static final String KEY_NAME="name";
    private static final String KEY_EMP_ADDRESS="address";
    SQLiteDatabase db;

    public DBHalper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " +TABLE_EMPLOYEE+ "(" +KEY_ID +"INTEGER PRIMARY KEY" +KEY_NAME +"TEXT, " +KEY_EMP_ADDRESS +" TEXT " + ")";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS"+TABLE_EMPLOYEE);
        onCreate(sqLiteDatabase);
    }
    public void addEmployee(Employee employee)
    {
        db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME,employee.getName());
        values.put(KEY_EMP_ADDRESS,employee.getAddress());
        values.put(KEY_ID,employee.getId());
        db.insert(TABLE_EMPLOYEE, null, values);
//        db.insert(TABLE_EMPLOYEE, null, values);
        db.close();
    }
    public Employee getEmployee(int id)
    {
       // SQLiteDatabase db;
        db=this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_EMPLOYEE, new String[]{KEY_ID,KEY_NAME,KEY_EMP_ADDRESS}, KEY_ID +"=?", new String[]{String.valueOf(id)}, null, null, null, null);
        Employee contact = null;

        if(cursor!=null)
        {
            cursor.moveToFirst();
            contact=new Employee(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
        }
        return contact;
    }

    public List<Employee> getAllEmployee()
    {
        List<Employee> employeeList=new ArrayList<>();
        String selectQuery="SELECT * FROM"+TABLE_EMPLOYEE;
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst())
        {
            do
            {
                Employee emp = new Employee();
                emp.setId(Integer.parseInt(cursor.getString(0)));
                emp.setName(cursor.getString(1));
                emp.setAddress(cursor.getString(2));
                employeeList.add(emp);
            }
            while (cursor.moveToNext());
        }
        return employeeList;


    }
}
