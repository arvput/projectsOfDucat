package com.example.vikashrajput.studentmanagmentsystem;

import android.app.ActionBar;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class StudentData extends AppCompatActivity {
    Button btnViewAll;
    TextView tvName,tvAddress,tvPhone;
    EditText editId;
    DBHandler myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_data);
        btnViewAll= (Button) findViewById(R.id.btnSubmit);
        tvName= (TextView) findViewById(R.id.tvName);
        tvAddress= (TextView) findViewById(R.id.tvAddress);
        tvPhone= (TextView) findViewById(R.id.tvPhone);
        editId= (EditText) findViewById(R.id.editId);
        myDb=new DBHandler(this);
        viewAll();
    }

    public void viewAll()
    {
        btnViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Cursor res= myDb.getAllData();

                if(res.getCount()==0)
                {
                    showMessage("Error ","nothing found");
                    return;
                }
                StringBuffer buffer=new StringBuffer();
                while (res.moveToNext())
                {
                    String id=editId.getText().toString();
                    tvName.setText("");
                    tvAddress.setText("");
                    tvPhone.setText("");
                /*    buffer.append("Id :"+res.getString(0)+"\n");
                    buffer.append("name :"+res.getString(1)+"\n");
                    buffer.append("address :"+res.getString(2)+"\n");
                    buffer.append("phone :"+res.getString(3)+"\n\n");*/
                    String dbid=res.getString(0);
                    String name=res.getString(1);
                    String address= res.getString(2);
                    String phone=res.getString(3);
                    if(id.equals(dbid))
                    {
                        tvName.setText("Name:-  "+name);
                        tvAddress.setText("Address:- "+address);
                        tvPhone.setText("Mob. No.:- "+phone);
                        break;
                    }
                }

                String msg=buffer.toString();
               // tvName.setText(msg);
                //showMessage("data",buffer.toString());
            }
        });
    }
    public void showMessage(String title,String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


}
