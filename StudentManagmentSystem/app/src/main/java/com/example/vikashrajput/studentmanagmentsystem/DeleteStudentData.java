package com.example.vikashrajput.studentmanagmentsystem;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DeleteStudentData extends AppCompatActivity {
    EditText editId;
    Button btnDelete;
    DBHandler myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_student_data);
        editId= (EditText) findViewById(R.id.editId);
        btnDelete= (Button) findViewById(R.id.btnDelete);
        myDb=new DBHandler(this);
        deleteData1();
        setTitle("Delet Students Data");

    }
    public void deleteData1()
    {
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Integer[] deleteRows = new Integer[1];

                AlertDialog.Builder builder = new AlertDialog.Builder(DeleteStudentData.this);
                builder.setCancelable(false);
                builder.setTitle(Html.fromHtml("<font color='#00000'>Do you want to Delete Data "));
                builder.setIcon(R.drawable.delete_image);
                builder.setPositiveButton("Delete ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteRows[0] = myDb.deleteData(editId.getText().toString());
                        if(deleteRows[0] > 0)
                        {
                            showDialog("Message !","Data Of student Id "+editId.getText().toString()+" Sussfully Deleted");
                        }
                        else
                            showDialog("Message !","Data Of student Id "+editId.getText().toString()+" Not Found");
                    }
                });
                builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });
                builder.show();

            }
        });
    }
    public void showDialog(String title,String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setIcon(R.drawable.delete_image);
        builder.setNegativeButton("Close",null);
        builder.show();
    }
}
