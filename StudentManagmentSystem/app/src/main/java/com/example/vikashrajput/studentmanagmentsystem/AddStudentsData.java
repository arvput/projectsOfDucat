package com.example.vikashrajput.studentmanagmentsystem;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddStudentsData extends AppCompatActivity {
    EditText editName,editAddress,editPhone,editId;
    Button btnAdd;
    DBHandler myDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_students_data);
        editId= (EditText) findViewById(R.id.editId);
        editName= (EditText) findViewById(R.id.editName);
        editAddress= (EditText) findViewById(R.id.editAddress);
        editPhone= (EditText) findViewById(R.id.editPhone);
        setTitle("Add Students Data");
        btnAdd= (Button) findViewById(R.id.btnAdd);
        myDb=new DBHandler(this);
        addData();
    }
    public void addData()
    {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final boolean[] isInserted = new boolean[1];
                AlertDialog.Builder builder = new AlertDialog.Builder(AddStudentsData.this);
                builder.setCancelable(false);
                builder.setTitle(Html.fromHtml("<font color='#000000'>Do you want to Save Data"));
                builder.setIcon(R.drawable.add_button);
                builder.setPositiveButton("Save ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        isInserted[0] = myDb.insertData(editId.getText().toString(),editName.getText().toString(),editAddress.getText().toString(),editPhone.getText().toString());
                        if(isInserted[0] ==true)
                        {
                            Toast.makeText(AddStudentsData.this, "Data Inserted ", Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(AddStudentsData.this, "Data not inserted ", Toast.LENGTH_SHORT).show();
                        AddStudentsData.this.finish();
                    }
                });
                builder.setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });
                builder.show();

            }
        });
    }
}
