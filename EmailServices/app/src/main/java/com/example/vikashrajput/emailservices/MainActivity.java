package com.example.vikashrajput.emailservices;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editTo,editSubject,editMessage;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editMessage= (EditText) findViewById(R.id.editMessage);
        editSubject= (EditText) findViewById(R.id.editSubject);
        editTo= (EditText) findViewById(R.id.editTo);
        btnSend= (Button) findViewById(R.id.button);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String t=editTo.getText().toString();
                String s=editSubject.getText().toString();
                String msg=editMessage.getText().toString();
                String[] to={t};
                String[] cc={" abc@gmail.com"};
                sendEmail(to,cc,s,msg);
            }
        });
    }
    protected void sendEmail(String[] emailAddress,String[] carbonCopies,String subject,String message)
    {
        Intent emailIntent=new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto: "));
        String[] to=emailAddress;
        String[] cc=carbonCopies;
        emailIntent.putExtra(Intent.EXTRA_EMAIL,to);
        emailIntent.putExtra(Intent.EXTRA_CC,cc);
        emailIntent.putExtra(Intent.EXTRA_TEXT,message);
        emailIntent.setType("message/utf_8");
        startActivity(Intent.createChooser(emailIntent,"Email"));
    }
}
